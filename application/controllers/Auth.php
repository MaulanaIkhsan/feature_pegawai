<?php
class Auth extends CI_Controller{
    
    public function index()
    {
        $this->load->view('pegawai/login');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

    public function action_login()
    {
        $this->load->model('User_sistem_model', 'user_sistem');

        $data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password'))
        );
        
        $check_user = $this->user_sistem->cek_user_login($data);
        print_r($check_user);

        if($check_user['respon']==='ok') {
            
            foreach ($check_user['result'] as $key => $value) {
                $this->session->set_userdata('user_sistem', $value->id);
                $this->session->set_userdata('pegawai', $value->pegawai_id);

                redirect('pages');
            }
        }
        if($check_user['respon']==='duplicate'){
            echo 'terjadi duplikasi data, harap hubungi admin sistem';
        }
        if($check_user['respon']==='empty'){
            echo 'data tidak ditemukan';
        }
    }

}