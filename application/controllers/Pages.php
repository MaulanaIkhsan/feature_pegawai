<?php
class Pages extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();

        $sess_user = $this->session->userdata('user_sistem');
        $sess_pegawai = $this->session->userdata('pegawai');

        if(($sess_user===NULL) && ($sess_pegawai===NULL)) {
            redirect('auth');
        }
    }
    public function view($page='home')
    {
        return 'function view';
    }

    public function index()
    {
        $this->load->view('pegawai/beranda');
    }

    public function login()
    {
        $this->load->view('pegawai/login');
    }
}
