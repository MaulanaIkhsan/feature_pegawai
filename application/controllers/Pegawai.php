<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Session 
		$sess_user = $this->session->userdata('user_sistem');
        $sess_pegawai = $this->session->userdata('pegawai');

        if(($sess_user===NULL) && ($sess_pegawai===NULL)) {
            redirect('auth');
        }

        $this->load->helper('form');
	}

	// List all your items
	public function index()
	{
		// Load important models
		$this->load->model('Pegawai_model', 'pegawai');

		$data['pegawai'] = $this->pegawai->fetch_data();

		$this->load->view('pegawai/show', $data);
	}

	public function show($id)
	{
		if(isset($id)){
			// Load important models
			$this->load->model('Pegawai_model', 'pegawai');

			$data['pegawai'] = $this->pegawai->detail_data($id);
			$this->load->view('pegawai/detail', $data);
		}
	}

	public function add_form()
	{
		// Load important models
		$this->load->model('Unit_kerja_model','unit_kerja');
		$this->load->model('Golongan_model', 'golongan');

		$data['unit_kerja'] = $this->unit_kerja->fetch_data();
		$data['golongan'] = $this->golongan->fetch_data();

		$this->load->view('pegawai/create', $data);

	}

	private function generate_filename()
	{
		return 'photo_'.date('Ymd').date('His');
	}

	// Add a new item
	public function add()
	{
		$this->load->model('Pegawai_model', 'pegawai');

		// Generate new filename
		$new_name = $this->generate_filename();

		// Setting upload file
		$config['upload_path'] = './assets/photos/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '500';
		$config['max_width'] = '1024';
		$config['max_height'] = '1024';
		$config['file_name'] = 'photo_'.date('Ymd').date('His');
		
		$this->load->library('upload', $config);

		// Get another inputed data
		$data['nama'] = $this->input->post('nama_lengkap');
		$data['unit_id'] = $this->input->post('unit_kerja');
		$data['golongan_id'] = $this->input->post('golongan');
		$data['jabatan'] = $this->input->post('jabatan');
		$data['jenis_kelamin'] = $this->input->post('kelamin');
		$data['no_telp'] = $this->input->post('telepon');
		$data['alamat'] = $this->input->post('alamat');
		
		// Rename photo
		$foto = $_FILES['foto']['name'];
		$split = explode('.', $foto);
		$data['foto'] = $new_name.'.'.$split[1]; //$split[1] is extension file

		$insert = $this->pegawai->create($data);
		$upload = $this->upload->do_upload('foto');
		if($insert==='ok' and $upload){
			redirect('pegawai');
		}
		else {
			print('input data pegawai is fail');
		}

	}

	//Update one item
	public function update($id)
	{
		// Load important models
		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Unit_kerja_model','unit_kerja');
		$this->load->model('Golongan_model', 'golongan');

		$data['unit_kerja'] = $this->unit_kerja->fetch_data();
		$data['golongan'] = $this->golongan->fetch_data();
		$data['pegawai'] = $this->pegawai->get_data_update($id);

		$this->load->view('pegawai/update', $data);
	}

	public function action_update($id)
	{
		if (isset($_POST['save'])) {
			$this->load->model('Pegawai_model', 'pegawai');

			// Generate new filename
			$new_name = $this->generate_filename();

			// Setting upload file
			$config['upload_path'] = './assets/photos/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '500';
			$config['max_width'] = '1024';
			$config['max_height'] = '1024';
			$config['file_name'] = 'photo_'.date('Ymd').date('His');
			
			$this->load->library('upload', $config);

			// Get another inputed data
			$data['nama'] = $this->input->post('nama_lengkap');
			$data['unit_id'] = $this->input->post('unit_kerja');
			$data['golongan_id'] = $this->input->post('golongan');
			$data['jabatan'] = $this->input->post('jabatan');
			$data['jenis_kelamin'] = $this->input->post('kelamin');
			$data['no_telp'] = $this->input->post('telepon');
			$data['alamat'] = $this->input->post('alamat');

			$key['id'] = $id;

			$foto = $_FILES['foto']['name'];
			
			if($foto) {
				// Get name photo
				$photo = $this->pegawai->get_name_photo($id);
				$file = './assets/photos/'.$photo->foto;

				if(file_exists($file)){
					// Rename photo
					$split = explode('.', $foto);
					$data['foto'] = $new_name.'.'.$split[1]; //$split[1] is extension file
					
					// Remove photo
					unlink($file);

					// Update data and reupload new photo
					$update = $this->pegawai->update($data, $key);
					$upload = $this->upload->do_upload('foto');
					if($update==='ok' and $upload){
						redirect('pegawai');
					}
					else{
						print('fail update / upload');
					}
				}
				else{
					print('photo is not found');
				}
			}
			else{
				$update = $this->pegawai->update($data, $key);
				if($update==='ok'){
					redirect('pegawai');
				}
				else{
					print('fail update');
				} 
			}

			// $split = explode('.', $foto);
			// $data['foto'] = $new_name.'.'.$split[1]; //$split[1] is extension file


		} 
		else{
			redirect('pegawai');
		}
	}

	//Delete one item
	public function delete($id)
	{
		// $file = './assets/photos/photo_20180515133815.png';
		// if (file_exists($file)) {
		// 	unlink($file);
		// 	print('delete success');
		// }
		// else{
		// 	print('cant be deleted');
		// }
		// print('pegawai id : '.$id);
		
		if(isset($id)) {
			$this->load->model('Pegawai_model','pegawai');
			// Get name photo
			$photo = $this->pegawai->get_name_photo($id);
			// Delete data pegawai
			$key['id'] = $id;
			// $delete = $this->pegawai->delete($key);
			// print_r($delete);

			$file = './assets/photos/'.$photo->foto;
			$delete = $this->pegawai->delete($key);
			if($delete['respon']==='ok'){
				if(file_exists($file)){
					unlink($file);
					redirect('pegawai');
				}
				else{
					print("photo's not exist");
				}
			}
			else{
				print("Delete data's fail");
			}
			
			// if($delete) {
			// 	$file = './assets/photos/photo_20180515133815.png';
			// 	if (file_exists($file)) {
			// 		unlink($file);
			// 		print('delete success');
			// 	}
			// 	else{
			// 		print('cant be deleted');
			// 	}
			// }
		}
	}

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */
