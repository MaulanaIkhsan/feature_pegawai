<?php
class User_sistem_model extends CI_Model {
    
    var $main_table = 'user_sistem';

    public function cek_user_login($data)
    {
        $checking = $this->db->select('id, pegawai_id')
                            ->from($this->main_table)
                            ->where('username', $data['username'])
                            ->where('password', $data['password'])
                            ->get();
                            
        $total_data = $checking->num_rows();
        $sess_data = array('respon'=>False);
        
        if($total_data === 1) {
            $sess_data['respon'] = 'ok';
            $sess_data['result'] = $checking->result();
        }
        else if($total_data > 1) {
            $sess_data['respon'] = 'duplicate';
        }
        else {
            $sess_data['respon'] = 'empty';
        }

        return $sess_data;
    }
}