<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Golongan_model extends CI_Model {

	var $main_table = 'golongan';

	public function fetch_data()
	{
		return $this->db->select('id, pangkat, tipe')
						->from($this->main_table)
						->order_by('pangkat, tipe')
						->get()->result();
	}

}

/* End of file Golongan_model.php */
/* Location: ./application/models/olongan_model.php */