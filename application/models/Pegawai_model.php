<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model {

	var $main_table = 'pegawai',
		$unit_kerja = 'unit_kerja',
		$golongan = 'golongan';

	public function fetch_data()
	{
		return $this->db->select('pegawai.id as id, pegawai.nama as nama, pegawai.unit_id as unit_id, unit_kerja.nama as unit_kerja, pegawai.golongan_id as golongan_id, golongan.pangkat as pangkat, golongan.tipe as tipe')
						->from($this->main_table)
						->join($this->unit_kerja, $this->main_table.'.unit_id = '.$this->unit_kerja.'.id', 'inner')
						->join($this->golongan, $this->main_table.'.golongan_id = '.$this->golongan.'.id', 'inner')
						->get()->result();
	}

	public function detail_data($id)
	{
		return $this->db->select('pegawai.id as id, pegawai.nama as nama, pegawai.unit_id as unit_id, unit_kerja.nama as unit_kerja, pegawai.golongan_id as golongan_id, golongan.pangkat as pangkat, golongan.tipe as tipe, pegawai.foto as foto, pegawai.jabatan as jabatan,pegawai.jenis_kelamin as jenis_kelamin, pegawai.no_telp as telepon, pegawai.alamat as alamat')
						->from($this->main_table)
						->join($this->unit_kerja, $this->main_table.'.unit_id = '.$this->unit_kerja.'.id', 'inner')
						->join($this->golongan, $this->main_table.'.golongan_id = '.$this->golongan.'.id', 'inner')
						->where('pegawai.id', $id)
						->get()->row();
	}

	public function create($datas)
	{
		if(isset($datas)) {
			$input = $this->db->insert($this->main_table, $datas);
			if ($input) {
				return 'ok';
			}
			else {
				return 'fail';
			}
		}
	}

	public function get_name_photo($id)
	{
		return $this->db->select('foto')
						->from($this->main_table)
						->where('id', $id)
						->get()->row();
	}

	public function delete($key)
	{
		$delete = $this->db->delete($this->main_table, $key);
		if($delete){
			return array('respon'=>'ok');
		}
		else{
			return array('respon'=>'fail');
		}
	}

	public function get_data_update($id)
	{
		return $this->db->select('id, nama, unit_id, golongan_id, jenis_kelamin, no_telp, alamat, jabatan')
						->from($this->main_table)
						->where('id', $id)
						->get()->row();
	}

	public function update($data, $key)
	{
		$update = $this->db->where($key)
						->update($this->main_table, $data);
		if ($update) {
			return 'ok';
		}
		else{
			return 'fail';
		}

	}

}

/* End of file Pegawai_model.php */
/* Location: ./application/models/Pegawai_model.php */