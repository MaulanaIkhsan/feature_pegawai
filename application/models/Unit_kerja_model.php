<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_kerja_model extends CI_Model {

	var $main_tabel = 'unit_kerja';

	public function fetch_unit_parent()
	{
		return $this->db->select('parent_id')
						->from('unit_kerja')
						->where('id is NOT NULL')
						->get()->result();
	}
	public function fetch_data()
	{
		// return $this->db->select('id, nama')
		// 				->from($this->main_tabel)
		// 				->where('id not in ('.$this->fetch_unit_parent().')')
		// 				->get()->result();
		/*
		revision sql : 
			SSELECT id,nama from unit_kerja WHERE id not in (SELECT parent_id from unit_kerja WHERE parent_id is NOT NULL) 
		*/
		$query = "SELECT id, nama from unit_kerja WHERE id not in (SELECT parent_id from unit_kerja WHERE parent_id is NOT NULL)";
		
		return $this->db->query($query)
						->result();
	}

}

/* End of file Unit_kerja_model.php */
/* Location: ./application/models/Unit_kerja_model.php */