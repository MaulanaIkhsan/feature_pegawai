<?php $this->load->view('templates/header'); ?>       
<!-- Form input and update data karyawan -->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
		    <div class="x_title">
		        <h2>Data Pegawai </h2>
		        <div class="clearfix"></div>
		    </div>
		    <div class="x_content">
        		<br />
        		<div class="table-responsive">
        			<a href="<?php print(base_url('pegawai/add_form')); ?>" class="btn btn-primary">Tambah Baru</a>
        			<table class="table table-striped jambo_table bulk_action">
        				<thead>
        					<tr class="headings">
        						<th class="column-title" align="center">No</th>
        						<th class="column-title" align="center">Nama</th>
        						<th class="column-title" align="center">Unit Kerja</th>
        						<th class="column-title" align="center">Golongan</th>
        						<th class="column-title" align="center">Aksi</th>
        					</tr>
        				</thead>
        				<tbody>
        					<?php $no=1; foreach($pegawai as $row):?>
        					<tr>
        						<td><?php print($no);?></td>
        						<td><?php print($row->nama);?></td>
        						<td><?php print($row->unit_kerja);?></td>
        						<td><?php print($row->pangkat.' '.$row->tipe);?></td>
        						<td align="center">
        							<a href="<?php print(base_url('pegawai/show/'.$row->id)); ?>" class="btn btn-primary">Detail</a> | 
        							<a href="<?php print(base_url('pegawai/update/'.$row->id)); ?>" class="btn btn-warning">Ubah</a> | 
        							<a href="#" class="btn btn-danger" data-target="#confirm-<?php print($no);?>" data-toggle="modal">Hapus</a>
                <!-- Modal confirmation delete -->
                <div class="modal fade" id="confirm-<?php print($no);?>" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel2">Konfirmasi Hapus</h4>
                      </div>
                      <div class="modal-body">
                        Apakah anda yakin menghapus data pegawai "<?php print($row->nama); ?>" ?
                      </div>
                      <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <a href="<?php print(base_url('pegawai/delete/'.$row->id));?>" class="btn btn-primary">Ya, Hapus</a>
                      </div>

                    </div>
                  </div>
                </div>
                <!-- /Modal confirmation logout -->
        						</td>
    
        					</tr>
			        		<?php $no++; endforeach;?>
        				</tbody>
        			</table>
	        		
        		</div>
        	</div>
		</div>

	</div>
</div>
<?php $this->load->view('templates/footer'); ?>  