<?php $this->load->view('templates/header'); ?>       
<!-- Form input and update data karyawan -->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
		    <div class="x_title">
		        <h2>Data Pegawai <?php print($pegawai->nama);?></h2>
		        <div class="clearfix"></div>
		    </div>
		    <div class="x_content">
        		<div class="col-md-3">
        			<img src="<?php print(base_url('/assets/photos/'.$pegawai->foto));?>" width="200" height="200"/>
        		</div>
        		<div class="col-md-9">
        			<div class="row" style="font-size: 16px;">
        				<div class="col-md-2">Nama</div>
        				<div class="col-md-1">:</div>
        				<div class="col-md-9"><?php print($pegawai->nama);?></div>
        				<div class="col-md-2">Unit Kerja</div>
        				<div class="col-md-1">:</div>
        				<div class="col-md-9"><?php print($pegawai->unit_kerja);?></div>
        				<div class="col-md-2">Golongan</div>
        				<div class="col-md-1">:</div>
        				<div class="col-md-9"><?php print($pegawai->pangkat.''.$pegawai->tipe);?></div>
        				<div class="col-md-2">Jabatan</div>
        				<div class="col-md-1">:</div>
        				<div class="col-md-9"><?php print($pegawai->jabatan);?></div>
        				<div class="col-md-2">Jenis Kelamin</div>
        				<div class="col-md-1">:</div>
        				<div class="col-md-9"><?php print($pegawai->jenis_kelamin);?></div>
        				<div class="col-md-2">Telepon</div>
        				<div class="col-md-1">:</div>
        				<div class="col-md-9"><?php print($pegawai->telepon);?></div>
        				<div class="col-md-2">Alamat</div>
        				<div class="col-md-1">:</div>
        				<div class="col-md-9"><?php print($pegawai->alamat);?></div>
        			</div>
        		</div>
        		<div class="col-md-12">
        			<a href="<?php print(base_url('pegawai/update/'.$pegawai->id));?>" class="btn btn-warning pull-right">Ubah data</a>
        			<a href="<?php print(base_url('pegawai'));?>" class="btn btn-danger pull-right">Kembali</a>
        		</div>
        	</div>
		</div>

	</div>
</div>
<?php $this->load->view('templates/footer'); ?>  