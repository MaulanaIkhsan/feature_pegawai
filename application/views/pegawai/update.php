<?php $this->load->view('templates/header'); ?>       
<!-- Form input and update data karyawan -->
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
    <div class="x_title">
        <h2>Ubah data pagawai </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="<?php print(base_url('pegawai/action_update/'.$pegawai->id));?>" method="post">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama-lengkap">Nama Lengkap <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="nama-lengkap" name="nama_lengkap" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $pegawai->nama; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit-kerja">Unit Kerja <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="unit_kerja" id="unit-kerja" required="required" class="form-control" >
                    <option value="">-- Pilih --</option>
                    <?php 
                    foreach($unit_kerja as $row):
                        if($row->id === $pegawai->unit_id) { ?>
                        <option value="<?php print($row->id); ?>" selected><?php print($row->nama); ?></option>
                        <?php } else {?>
                        <option value="<?php print($row->id); ?>"><?php print($row->nama); ?></option>
                    <?php } endforeach;?>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan">Golongan <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="golongan" id="golongan" required="required" class="form-control" >
                    <option value="">-- Pilih --</option>
                    <?php foreach($golongan as $row):
                            if($row->id === $pegawai->golongan_id) {?>
                        <option value="<?php print($row->id); ?>" selected><?php print($row->pangkat.' '.$row->tipe); ?></option>
                        <?php } else {?>
                        <option value="<?php print($row->id); ?>"><?php print($row->pangkat.' '.$row->tipe); ?></option>
                    <?php } endforeach;?>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan">Jabatan <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="jabatan" id="jabatan" required="required" class="form-control" >
                    <option value="">-- Pilih --</option>
                    <?php if($pegawai->jabatan === 'Kepala'){?>
                        <option value="kepala" selected>Kepala</option>
                        <option value="staff">Staff</option>
                    <?php } else { ?>
                        <option value="kepala">Kepala</option>
                        <option value="staff" selected>Staff</option>
                    <?php } ?>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis-kelamin">Jenis Kelamin<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <p>
                    <div> 
                        <?php if($pegawai->jenis_kelamin === 'Pria'){?>
                            Pria : <input class="flat" type="radio" id="kelaminP" name="kelamin" required="required" value="pria" checked="checked" /> 
                            Wanita :  <input class="flat" type="radio" id="kelaminW" name="kelamin" required="required" value="wanita" />
                        <?php } else { ?>
                            Pria : <input class="flat" type="radio" id="kelaminP" name="kelamin" required="required" value="pria"  /> 
                            Wanita :  <input class="flat" type="radio" id="kelaminW" name="kelamin" required="required" value="wanita" checked="checked" />
                        <?php } ?>
                        
                    </div>                               
                </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor-telepon">Nomor Telepon</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nomor-telepon" data-inputmask="'mask' : '999999999999'" name="telepon" required="required" class="form-control col-md-7 col-xs-12" value="<?php print($pegawai->no_telp);?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor-telepon">Alamat</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea name="alamat" cols="10" rows="3" class="form-control"><?php print($pegawai->alamat); ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor-telepon">Foto Baru</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" name="foto" class="form-group" />
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a href="<?php print(base_url('pegawai')); ?>" class="btn btn-danger" >Batal</a>
                    <button type="submit" class="btn btn-success" name="save">Simpan</button>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>
</div>
<!-- /Form input and update data karyawan -->
<?php $this->load->view('templates/footer'); ?>