<?php $this->load->view('templates/header'); ?>       
<!-- Form input and update data karyawan -->
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
    <div class="x_title">
        <h2>Form Input & Edit Data Pegawai </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama-lengkap">Nama Lengkap <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="nama-lengkap" name="nama-lengkap" required="required" class="form-control col-md-7 col-xs-12" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit-kerja">Unit Kerja <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="unit-kerja" id="unit-kerja" required="required" class="form-control" >
                <option name="">-- Pilih --</option>
                <option name="kepala-dinas">Kepala Dinas</option>
                <option name="bendahara">Bendahara</option>
                <option name="sekertaris">Sekertaris</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan">Golongan <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="golongan" id="golongan" required="required" class="form-control" >
                <option name="">-- Pilih --</option>
                <option name="I">I</option>
                <option name="II">II</option>
                <option name="III">III</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis-kelamin">Jenis Kelamin<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <p>
                <div> 
                    Pria : <input class="flat" type="radio" id="kelaminP" name="kelamin" required="required" value="pria"  /> 
                    Wanita :  <input class="flat" type="radio" id="kelaminW" name="kelamin" required="required" value="wanita" />
                </div>                               
            </p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor-telepon">Nomor Telepon</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="nomor-telepon" data-inputmask="'mask' : '999999999999'" name="telepon" required="required" class="form-control col-md-7 col-xs-12" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor-telepon">Alamat</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="alamat" cols="10" rows="3" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor-telepon">Foto</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" name="foto" class="form-group" />
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="reset" class="btn btn-primary" >Reset</button>
            <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </div>

        </form>
    </div>
    </div>
</div>
</div>
<!-- /Form input and update data karyawan -->
<?php $this->load->view('templates/footer'); ?>