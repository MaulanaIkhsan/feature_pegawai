<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <!-- top navbar -->
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"> <span>Feature Pegawai</span></a>
        </div>
        <div class="clearfix"></div>
        <br />
        <!-- /top navbar -->

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
            <h3>General</h3>
            <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i> Home</a></li>
                <li></li>
                <li><a><i class="fa fa-database"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php print(base_url('pegawai'));?>">Pegawai</a></li>
                        <li><a href="#">Unit Kerja</a></li>
                        <li><a href="#">Golongan</a></li>
                    </ul>
                </li>
                <li>
                    <a data-toggle="modal" data-toggle="modal" data-target=".bs-example-modal-sm">
                        <i class="fa fa-sign-out"></i> Logout
                    </a>
                </li>                  
            </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

       

    </div>
</div>