        <!-- /page content -->
                </div>
            </div>
        </div>
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
        
      </div>
    </div>
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/gentelella/vendors/jquery/dist/jquery.min.js');?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/gentelella/vendors/fastclick/lib/fastclick.js');?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url('assets/gentelella/vendors/nprogress/nprogress.js');?>"></script>
    <!-- jQuery custom content scroller -->
    <script src="<?php echo base_url('assets/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js');?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('assets/gentelella/vendors/iCheck/icheck.min.js');?>"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo base_url('assets/gentelella/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js');?>"></script>
        
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/gentelella/build/js/custom.min.js');?>"></script>
  </body>
</html>